var gulp = require('gulp');

var jimp = require("gulp-jimp-resize");
const imagemin = require('gulp-imagemin');

gulp.task('header-images', function() {
    return gulp.src(
        'src/images/*.{png,jpg,bmp}'
        )
    .pipe(jimp({
        sizes: [
            {"suffix": "320", "width": 320},
            {"suffix": "640", "width": 640},
            {"suffix": "1280", "width": 1280},
        ]
    }))
    .pipe(imagemin({
		interlaced: true,
		progressive: true,
		optimizationLevel: 5,
		svgoPlugins: [{removeViewBox: true}]
	}))
    .pipe(gulp.dest('src/images/'));
});

gulp.task('logo', function() {
    return gulp.src(
        'src/logo.png'
        )
    .pipe(jimp({
        sizes: [
            {"suffix": "120", "width": 120}
        ]
    }))
    .pipe(gulp.dest('src/'));
});