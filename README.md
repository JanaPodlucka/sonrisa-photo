Live demo is available on https://sonrisa-photo.herokuapp.com/

To run the app locally:

git clone https://JanaPodlucka@bitbucket.org/JanaPodlucka/sonrisa-photo.git

cd sonrisa-photo

npm install

npm start

open http://localhost:3000