import React from 'react';
import Icon from 'react-simple-icons';
import './SocialIcons.css';

export const SocialIcons = () => {
	return (
		<div className="social-icons">
			<a href='https://www.facebook.com/zuzu.ant1' className="social-icons__link"><Icon name='facebook' fill='white' size={24}/></a>
      		<a href='https://www.instagram.com/zuzantt' className="social-icons__link"><Icon name='instagram' fill='white' size={24} /></a>
		</div>
	);
}