import React from 'react';
import PropTypes from 'prop-types';

import './Image.css';

export const Image = (props) => {
	let background = { backgroundImage: 'url(' + props.url + ')' };
	
	return(
		<div style={background} className="image"></div>
		);
  
}

	Image.propTypes = {
		url: PropTypes.string.isRequired,
		alt: PropTypes.string
	}
