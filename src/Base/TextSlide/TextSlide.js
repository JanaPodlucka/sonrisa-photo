import React from 'react';
import './TextSlide.css';
import PropTypes from 'prop-types';

export const TextSlide = (props) => {
	return(
		<p className={"text-slide " + (props.className)}>{props.text}</p>
	);
}

TextSlide.propTypes = {
	className : PropTypes.string,
	text : PropTypes.string.isRequired
}

