import React from 'react';
import PropTypes from 'prop-types';
import './Parallax.css';

export const Parallax = (props) => {
	const styles = {
		backgroundImage: 'url(' + props.url + ')'
	};
	return(
		<div className="parallax" style={styles}></div>
		);
  
}

	Parallax.propTypes = {
		url: PropTypes.string.isRequired
	}
