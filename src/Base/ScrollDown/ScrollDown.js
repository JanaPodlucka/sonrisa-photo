import React, {Component} from 'react';
import Scroll from 'react-scroll';
import './ScrollDown.css'

class ScrollDown extends Component {
	constructor(props) {
	  super(props);
	  this.state = { height: '0' };
	  this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	  this.handleClick = this.handleClick.bind(this);
	}

	componentDidMount() {
	  this.updateWindowDimensions();
	  window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
	  window.removeEventListener('resize', this.updateWindowDimensions);
	}

	updateWindowDimensions() {
	  this.setState({ height: window.innerHeight });
	}

	handleClick() {
		let scroll = Scroll.animateScroll;
		scroll.scrollTo(this.state.height);
	}

	render() {
		return(
			<div className="scroll-down">
				<span onClick={this.handleClick} className="scroll-down__icon">
				</span>
			</div>
		);
	}
}

export default ScrollDown;