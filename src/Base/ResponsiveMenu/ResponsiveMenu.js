import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './ResponsiveMenu.css';

class ResponsiveMenu extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	      showMenu : false
	    }
	};

	componentWillMount() {
    	document.addEventListener('click', this.handleClick, true);
  	}

	componentWillUnmount() {
	   document.removeEventListener('click', this.handleClick, true);
  	}

	handleClick = (e) => {

		if(!(ReactDOM.findDOMNode(this).contains(e.target))) {
		   	 this.setState({showMenu : false});
		}
		//TO DO: ak kliknem na aktuálny link, tak this.setState({showMenu : false});
	}
	  
	handleMenuClick = () => {
	    this.setState({showMenu : !this.state.showMenu});
	}

	render() {
		return (
			<nav className={"header-navigation" + (this.state.showMenu ? " responsive-menu" : "")}>
				{this.props.children}
				<div className="responsive-menu__icon" onClick={this.handleMenuClick}>
		          <span className="responsive-menu__icon-line"></span>
		          <span className="responsive-menu__icon-line"></span>
		          <span className="responsive-menu__icon-line"></span>
		        </div>
		    </nav>
		);
	}
}

export default ResponsiveMenu;