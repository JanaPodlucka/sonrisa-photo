import React from 'react';
import PropTypes from 'prop-types';

import './TwoColumnSection.css';

export const TwoColumnSection = (props) => {
	return(
		<section className="two-column-section">
			<div className="two-column-section__left">{props.left}</div>
			<div className="two-column-section__right">{props.right}</div>
		</section>
		);
  
}

	TwoColumnSection.propTypes = {
		left: PropTypes.object.isRequired,
		right: PropTypes.object.isRequired
	}
