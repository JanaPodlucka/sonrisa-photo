import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Slide.css'

class Slide extends Component {
	constructor() {
    super();
    this.state = {
      width: window.innerWidth,
    };
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  render() {
    const width = this.state.width;
    let background;
    if ( width > 1280) background = { backgroundImage: 'url(' + this.props.url.default + ')' };
    else if (width > 640) background = { backgroundImage: 'url(' + this.props.url.img_1280 + ')' };
    else if (width > 320) background = { backgroundImage: 'url(' + this.props.url.img_640 + ')' };
    else background = { backgroundImage: 'url(' + this.props.url.img_320 + ')' };
  	
    return(
  			<div style={background} className="slide"></div>
  		);
  	
  }
}
export default Slide;

Slide.propTypes = {
		url: PropTypes.object.isRequired
}