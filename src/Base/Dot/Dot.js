import React from 'react';
import './Dot.css';
import PropTypes from 'prop-types';

export const Dot = (props) => {
	return <li className={'dot ' + props.className} onClick={() => props.change(props.id)}></li>;
  
}

	Dot.propTypes = {
		id: PropTypes.number.isRequired,
		className: PropTypes.string
	}
