import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './GalleryFolder.css';

export const GalleryFolder = (props) => {
	const background = { backgroundImage: 'url(' + props.url + ')' };
	return (
		<Link to={`/gallery/${props.path}`} className='gallery-folder-link' >
			<div className="gallery-folder" style={background}>
			{ props.title && <h1 className="gallery-folder__title">{props.title}</h1> }
			</div>
		</Link>
	);
}


GalleryFolder.propTypes = {
	url : PropTypes.string.isRequired,
	path : PropTypes.string.isRequired,
	title : PropTypes.string.isRequired
}