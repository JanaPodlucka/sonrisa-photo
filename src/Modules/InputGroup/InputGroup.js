import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './InputGroup.css';

class InputGroup extends Component {
	constructor(props){
		super(props);
		
		this.state = {
			filled : false
		};
	}

	onChange = (e) => {
		let inputValue = e.target.value;
		this.setState({
			filled : inputValue.length > 0
		});
	} 

	render() {
		let output;
		
		if (this.props.textarea) 
			output = <textarea id = { this.props.id } className="input-group__textarea" onChange={this.onChange}></textarea>;
		else
			output = <input id = { this.props.id } type = { this.props.type} className="input-group__input" onChange={this.onChange}/>;
		
		return(
			<div className="input-group">		
				{ output }
				<label className={"input-group__label " + (this.state.filled ? "input-group__label--filled" : "")}>{this.props.placeholder}</label>
			</div>
		);
	}
}

export default InputGroup;

InputGroup.propTypes = {
		id : PropTypes.string.isRequired,
		type : PropTypes.string,
		placeholder : PropTypes.string.isRequired,
		textarea : PropTypes.bool
	}

InputGroup.defaultProps = {
	textarea : false
}