import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group';
import PropTypes from 'prop-types';
import Slide from '../../Base/Slide/Slide';
import { TextSlide } from '../../Base/TextSlide/TextSlide';
import './Slider.css';
import DotsContainer from '../DotsContainer/DotsContainer';
import ScrollDown from '../../Base/ScrollDown/ScrollDown';

const changeInterval = 3500;


class Slider extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: 0,
			interval : null
		};
		this.array = this.props.array;		
	}
	

	setSliderInterval = (id) => {
		const that = this;
		this.setState({id : id}, function(){
			let interval = setInterval(function(){
				let id = (that.state.id + 1) % that.array.length;
				that.setState({id: id});
			}, changeInterval);
			this.setState({interval : interval});
		});
	}


	changeSlide = (id) => {
		clearInterval(this.state.interval);
		this.setSliderInterval(id);
	}

	componentWillMount() {
		this.setSliderInterval(this.state.id);
	}

	componentWillUnmount() {
		clearInterval(this.state.interval);
	}

	render() {
		const that = this;
		let output;
		if ( this.props.type === "text" ) {
			output = this.array.map(function(item, i) {
				return (that.state.id === i && <TextSlide text={item} key={i} className={that.props.className} />);
			});
		}
		else {
			output = this.array.map(function(item, i) {
				return (that.state.id === i && <Slide url={item} key={i} className={that.props.className}/>);
			});
		}

		return (
			<div className="slider">
				<CSSTransitionGroup
	                transitionName="slider-slide"
	                transitionEnterTimeout={1500}
	                transitionLeaveTimeout={800} >
                {output}
                {this.props.scroll && <ScrollDown /> }
            	</CSSTransitionGroup>
				{this.props.dots && <DotsContainer id={this.state.id} array={this.array} change={this.changeSlide} />}
			</div>
			);
		}
	}

	Slider.propTypes = {
		dots: PropTypes.bool,
		array: PropTypes.array.isRequired,
		type: PropTypes.string,
		className: PropTypes.string
	}

	Slider.defaultProps = {
		dots : true,
		scroll : false
	};

	export default Slider;