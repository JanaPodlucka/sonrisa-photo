import React from 'react';
import Slider from '../Slider/Slider';
import './ThingsAboutMe.css';

export const ThingsAboutMe = () => {
	const thingsAboutMe = [
	"Gummies pie tart cake dragée muffin caramels dragée topping. Danish tootsie roll marzipan pastry muffin pudding muffin chocolate cake caramels.",
	"Cake toffee chocolate cake halvah. Macaroon pudding powder tiramisu jelly beans. Fruitcake macaroon soufflé. Candy biscuit sesame snaps chocolate bar fruitcake.",
	"Sweet roll marzipan pie oat cake ice cream donut. Danish dragée pie chocolate icing liquorice jelly. Soufflé liquorice sweet bear claw pie jelly-o cookie."
	];

	return(
		<section className="things-about-me">
			<h1 className="things-about-me__header">{thingsAboutMe.length} Things About Me</h1>
			<h2 className="things-about-me__subheader">You Should Probably Know</h2>
			<Slider array={ thingsAboutMe } type="text" className="things-about-me__content" />
		</section>
		);
}