import React from 'react';
import './Footer.css';
import { Logo } from '../../Base/Logo/Logo';
import { Menu } from '../../Modules/Menu/Menu';
import { SocialIcons } from '../../Base/SocialIcons/SocialIcons';

export const Footer = () => {
	return(
		<footer>
			<Logo />
			<Menu className="menu--footer" />
			<SocialIcons />
		</footer>
		);
  
}