import React, { Component } from 'react';
import { Dot } from '../../Base/Dot/Dot';
import PropTypes from 'prop-types';
import './DotsContainer.css'

class DotsContainer extends Component {
  constructor(props) {
    super(props);

    this.array = this.props.array;
  };
  
  render() {

    const that = this;
    const output = this.props.array.map(function(item, i) {
      let name;
      (i===that.props.id) ? name = "dot--selected" : name = null;
          
      return (<Dot className={ name } change={that.props.change} key={i} id={i}/>);
    });
    
    return (
      <ul className="dots-container">
        {output}
      </ul>
    );
  }
} 

export default DotsContainer;

DotsContainer.propTypes = {
  array : PropTypes.array.isRequired,
  id : PropTypes.number.isRequired,
  change : PropTypes.func.isRequired
}