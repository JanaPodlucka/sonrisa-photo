import React from 'react';
import { Footer } from '../Footer/Footer';
import { Header } from '../Header/Header';
import PropTypes from 'prop-types';
import './PageWrapper.css';

export const PageWrapper = (props) => {
		return(
		<div>
			<Header modifier={ props.headerModifier } images={ props.images } menuModifier={ props.menuModifier } />
			<main>
				<h1 className="page-wrapper-title">{props.title}</h1>
				{props.children}
			</main>
			<Footer />
		</div>

	);
}

PageWrapper.propTypes = {
	title : PropTypes.string,
	headerModifier : PropTypes.string,
	menuModifier : PropTypes.string,
	images : PropTypes.array,
	children : PropTypes.node
}