import React from 'react';
import Slider from '../Slider/Slider';
import './Header.css';
import ResponsiveMenu from '../../Base/ResponsiveMenu/ResponsiveMenu';
import { Menu } from '../Menu/Menu';

export const Header = (props) => {
	let output = null;

	if (props.images != null)
		output = (
			<Slider array={props.images} dots={false} scroll={true} />
			);
	return(
		<header className={'header ' + props.modifier}>
			<ResponsiveMenu >
				<Menu className={props.menuModifier} showLogo={true} />
			</ResponsiveMenu>
			{ output }
		</ header>
	);
}


Header.defaultProps = {
	images : null
}