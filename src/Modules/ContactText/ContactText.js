import React from 'react';
import { SocialIcons } from '../../Base/SocialIcons/SocialIcons';
import "./ContactText.css";

export const ContactText = () => {
	return(
		<section className="contact">
			<h1 className="contact__header">Contact me</h1>
			<p className="contact__content">
				Cupcake ipsum dolor sit amet. Oat cake sweet powder cupcake sweet I love sweet marzipan. Gummies cake chocolate cake sesame snaps halvah. Dragée I love sweet cake liquorice sugar plum I love. Macaroon caramels cheesecake fruitcake tootsie roll dragée cake I love. Chocolate sweet sweet roll pastry chocolate cotton candy donut muffin sweet roll.
			</p>

			<ul className="contact__box">
				<li className="contact__box-item">
					<svg fill="#FFFFFF" height="36" viewBox="0 0 24 24" width="36" xmlns="http://www.w3.org/2000/svg">
					    <path d="M15.5 1h-8C6.12 1 5 2.12 5 3.5v17C5 21.88 6.12 23 7.5 23h8c1.38 0 2.5-1.12 2.5-2.5v-17C18 2.12 16.88 1 15.5 1zm-4 21c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm4.5-4H7V4h9v14z"/>
					    <path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					<h1 className="contact__box-name">Call me</h1>
					<p className="contact__box-value">+421000 000 000</p>
				</li>
				<li className="contact__box-item">
					<svg fill="#FFFFFF" height="36" viewBox="0 0 24 24" width="36" xmlns="http://www.w3.org/2000/svg">
					    <path d="M0 0h24v24H0z" fill="none"/>
					    <path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z"/>
					</svg>
						<h1 className="contact__box-name">Email me</h1>
					<p className="contact__box-value">aaa@aaa.aaa</p>
				</li>
			</ul>
			<SocialIcons />
		</section>
	);
}