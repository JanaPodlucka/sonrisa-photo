import React from 'react';
import logo from '../../logo.png';
import './Menu.css';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export const Menu = (props) => {
	return (
		<ul className={"menu " + props.className}>
			<li className="menu__item"><Link to='/' className="menu__link" data-hover="Domov">Domov</Link></li>
			<li className="menu__item"><Link to='/gallery' className="menu__link" data-hover="Galéria">Galéria</Link></li>
		    {props.showLogo && <li className="menu__logo"><Link to='/'><img className="menu__logo" src={logo} alt='logo'/></Link></li>}
		    <li className="menu__item"><Link to='/aboutme' className="menu__link" data-hover="O mne">O mne</Link></li>
		    <li className="menu__item"><Link to='/contact' className="menu__link" data-hover="Kontakt">Kontakt</Link></li>
	    </ul>
	);
}

Menu.propTypes = {
	className : PropTypes.string.isRequired,
	showLogo: PropTypes.bool
}

Menu.defaultProps = {
	showLogo: false
}