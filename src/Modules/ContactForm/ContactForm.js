import React, { Component } from 'react';
import './ContactForm.css';
import InputGroup from '../InputGroup/InputGroup';

class ContactForm extends Component {
	
	onClick = (e) => {
		e.preventDefault();
	}
	
	render() {
		return(
			<form className="contact-form">
				<InputGroup id="name" type="text" placeholder="Name"/>
				<InputGroup id="email" type="email" placeholder="Email"/>
				<InputGroup id="message" placeholder="Message" textarea={true} />
				<input type="submit" value="Send" className="contact-form__submit" onClick={ this.onClick }/>
			</form>
		);
	}
}

export default ContactForm;