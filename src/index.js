import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import './Main.css';
import Home from './Pages/Home';
import { NotFound } from './Pages/NotFound';
import { Gallery } from './Pages/Gallery';
import { About } from './Pages/About';
import Contact from './Pages/Contact';
import { GalleryFolderImages } from './Pages/GalleryFolderImages';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

ReactDOM.render(
	<BrowserRouter>
		<div>
		    <Switch>
		    	<Route exact path="/" component={Home} />
		    	<Route exact path="/gallery" component={Gallery} />
		    	<Route exact path="/gallery/:id" component={GalleryFolderImages} />
		    	<Route exact path="/aboutme" component={About} />
		    	<Route exact path="/contact" component={Contact} />
		    	<Route component={NotFound}/>
			</Switch>
		 </div>
	</BrowserRouter>
	, document.getElementById('root'));
registerServiceWorker();
