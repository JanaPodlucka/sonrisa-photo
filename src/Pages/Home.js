import React, { Component } from 'react';
import { PageWrapper } from '../Modules/PageWrapper/PageWrapper';
import { TwoColumnSection } from '../Base/TwoColumnSection/TwoColumnSection';
import { Image } from '../Base/Image/Image';
import { AboutMe } from '../Base/AboutMe/AboutMe';
import { Parallax } from '../Base/Parallax/Parallax';


import img1 from '../images/1.jpg';
import img1_320 from '../images/1-320.jpg';
import img1_640 from '../images/1-640.jpg';
import img1_1280 from '../images/1-1280.jpg';
import img2 from '../images/2.jpg';
import img2_320 from '../images/2-320.jpg';
import img2_640 from '../images/2-640.jpg';
import img2_1280 from '../images/2-1280.jpg';
import img3 from '../images/3.jpg';
import img3_320 from '../images/3-320.jpg';
import img3_640 from '../images/3-640.jpg';
import img3_1280 from '../images/3-1280.jpg';

const headerImages = [
                {default: img1, img_320: img1_320,img_640: img1_640, img_1280: img1_1280},
                {default: img2, img_320: img2_320,img_640: img2_640, img_1280: img2_1280},
                {default: img3, img_320: img3_320,img_640: img3_640, img_1280: img3_1280} 
               ];

class Home extends Component {

	render() {
			const left = <Image url='http://www.focusit.ca/wp-content/uploads/Girl-Photographer.jpg'/>;
			const right = <AboutMe />;
		return(
			<PageWrapper headerModifier="home-header" menuModifier="menu--home-header" images={ headerImages }>
					<TwoColumnSection left={left} right={right}/>
					<Parallax url="https://s-i.huffpost.com/gen/1551096/images/o-HAPPY-FAMILY-facebook.jpg"/>
			</ PageWrapper>
		);
	}
}

export default Home;