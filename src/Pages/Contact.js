import React, { Component } from 'react';
import { PageWrapper } from '../Modules/PageWrapper/PageWrapper';
import { TwoColumnSection } from '../Base/TwoColumnSection/TwoColumnSection';
import ContactForm from '../Modules/ContactForm/ContactForm';
import { ContactText } from '../Modules/ContactText/ContactText'; 

class Contact extends Component {


	render(){
		const left = <ContactText />;
		const right = <ContactForm />;
		return (
			<PageWrapper title="Contact" menuModifier="menu--header">
				<TwoColumnSection left={left} right={right}/>
			</PageWrapper>
			);
	}
}

export default Contact;