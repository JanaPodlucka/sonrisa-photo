import React from 'react';
import { PageWrapper } from '../Modules/PageWrapper/PageWrapper';
import { Image } from '../Base/Image/Image';
import { TwoColumnSection } from '../Base/TwoColumnSection/TwoColumnSection';
import { ThingsAboutMe } from '../Modules/ThingsAboutMe/ThingsAboutMe';

export const About = () => {
		const left = <Image url='http://www.focusit.ca/wp-content/uploads/Girl-Photographer.jpg'/>;
		const right = <ThingsAboutMe />;
	return (
		<PageWrapper title="About me" menuModifier="menu--header">
			<TwoColumnSection left={left} right={right}/>
		</PageWrapper>

		);
}