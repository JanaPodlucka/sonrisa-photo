import React from 'react';
import { PageWrapper } from '../Modules/PageWrapper/PageWrapper';
import { GalleryFolder } from '../Base/GalleryFolder/GalleryFolder';

export const Gallery = () => {
	return(
		<PageWrapper title="Gallery" menuModifier="menu--header">
			{/*TO DO: doplniť reálne obrázky*/}
			<GalleryFolder url="http://epartnersinlearning.org/wp-content/uploads/2015/08/help_children_develop_their_talents_and_creativity_via_play.jpg" title="Image folder" path="images1"/>
			<GalleryFolder url="https://i.pinimg.com/736x/d8/cd/96/d8cd965dc305ea0642bf78db9dcea2a3--flower-girl-crown-flower-girls.jpg" title="Image folder" path="images2"/>
		</PageWrapper>
	); 
} 