import React from 'react';
import { PageWrapper } from '../Modules/PageWrapper/PageWrapper';

export const NotFound = () => {

	return (
		<PageWrapper title="Sorry, this page is not found :(" menuModifier="menu--header">
		</PageWrapper>
	);
}
